
| ![输入图片说明](https://cdn.weiunity.com/site/341/news/6659bb2111794cd8b70c90616cbd434b.png "在这里输入图片标题")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/171852_eab00bf0_429922.png "在这里输入图片标题") | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/172305_300ad448_429922.png "在这里输入图片标题") |
|---|---|---|

## 简介
H5做的在线客服对话窗口，类似百度商桥。结合后端功能有：
1. 支持手机端、电脑端。
1. 聊天记录使用ELK持久化保存。支撑巨量聊天记录无延迟
1. 支持离线消息。上线后自动接收离线消息
1. 聊天中,上滑可无感加载历史聊天记录
1. 用户打开聊天窗口时自动给用户发送欢迎语、常见问题
1. 当用户发送消息，但客服不在线时，会自动发送设置好的一段文字给用户
1. 对已录入的常见问题进行自动匹配、自动回复。
1. 本地消息存储，最新消息的本地缓存，极致流畅度
1. 支持图片上传、发送图片、图片放大。
1. 支持表情
1. 新消息提醒。当我跟A用户正在聊天，B用户给我发来消息，顶部实时显示别的用户发来的新消息提醒
1. 支持自定义插件，如发送订单、更多自定义表情、发送商品、发送位置、文件、名片、礼物……都可以用插件方式扩展。
1. 支持自定义心跳及心跳包扩展
1. 支持断线自动重连


## 快速体验
可以进入我们这个客服项目的网站，快速测试体验：  
[www.kefu.zvo.cn](http://www.kefu.zvo.cn)

## 使用及开发
#### 懒人极速体验代码
新建一个HTML文件，其内容如下，直接用浏览器打开即可看到效果。适用于懒人。
````
<!DOCTYPE html>
<html lang="zh-cn"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>DEMO</title>
</head>
<body>
<script src="https://res.weiunity.com/wm/wm.js"></script>
<script src="https://res.weiunity.com/kefu/dev/h5Pc/h5Pc.js"></script>
<button onclick="kefu.ui.chat.entry('365fef747a9e493fb631b621ee36eed1');">打开PC端客服聊天窗口</button>
</body>
</html>
````

更多更详细的文档，已托管在码云文档，请看下面
#### 码云托管的前端扩展及开发的详细文档
[![输入图片说明](http://cdn.weiunity.com/site/5348/news/8c53da0c730b4054b12f4ec629dbf7a5.png "在这里输入图片标题")](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/kefujs-api-interface-of/preview?sort_id=3663196&doc_id=1274007)
文档地址：  
https://gitee.com/leimingyun/dashboard/wikis/leimingyun/kefujs-api-interface-of/preview?sort_id=3663196&doc_id=1274007

## 客服系统后端服务的Java源代码
https://gitee.com/leimingyun/yunkefu

## 与第三方系统对接，API接口文档
如果您已经有现有的系统，想给自己的系统增加一个在线客服功能模块，你可以快速利用本客服系统提供的API接口，快速将云客服嵌入到您自己的系统中去。API文档可参考：  
https://e.gitee.com/leimingyun/doc/share/60f7bc4cb4fb2338/?sub_id=7957633

## 系统安装到自己服务器，安装文档
https://e.gitee.com/leimingyun/doc/share/60f7bc4cb4fb2338/?sub_id=7954273  

## 其他
作者：管雷鸣  
微信：xnx3com  
产品官网：www.kefu.zvo.cn  
公司官网：www.leimingyun.com  
QQ交流群：763362335  
  
已有知识产权保护，如果用于商业用途，没事，随便用吧！！  


## 开源项目

致力于开源基础化信息建设，如有需要，可直接拿去使用。这里列出了我部分开源项目：

| 项目| star数量 | 简介 |  
| --- | --- | --- |
|[wangmarket CMS](https://gitee.com/mail_osc/wangmarket) | ![](https://gitee.com/mail_osc/wangmarket/badge/star.svg?theme=white) | [私有部署自己的SAAS建站系统](https://gitee.com/mail_osc/wangmarket)  |
|[obs-datax-plugins](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins) | ![](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins/badge/star.svg?theme=white) | [Datax 的 华为云OBS 插件](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins) |
| [templatespider](https://gitee.com/mail_osc/templatespider) | ![](https://gitee.com/mail_osc/templatespider/badge/star.svg?theme=white) | [扒网站工具，所见网站皆可为我所用](https://gitee.com/mail_osc/templatespider) |
|[FileUpload](https://gitee.com/mail_osc/FileUpload)| ![](https://gitee.com/mail_osc/FileUpload/badge/star.svg?theme=white ) | [文件上传，各种存储任意切换](https://gitee.com/mail_osc/FileUpload) |
| [cms client](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms) | ![](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms/badge/star.svg?theme=white) | [云服务深度结合无服务器建站](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms)  |
| [kefu.js](https://gitee.com/mail_osc/kefu.js) | ![](https://gitee.com/mail_osc/kefu.js/badge/star.svg?theme=white ) | https://gitee.com/mail_osc/kefu.js | [在线聊天的前端框架](https://gitee.com/mail_osc/kefu.js)  | 
| [msg.js](https://gitee.com/mail_osc) | ![](https://gitee.com/mail_osc/msg/badge/star.svg?theme=white ) | [轻量级js消息提醒组件](https://gitee.com/mail_osc)  | 
| [translate.js](https://gitee.com/mail_osc/translate) | ![](https://gitee.com/mail_osc/translate/badge/star.svg?theme=white )  | [三行js实现 html 全自动翻译](https://gitee.com/mail_osc/translate)  | 
| [WriteCode](https://gitee.com/mail_osc/writecode) | ![](https://gitee.com/mail_osc/writecode/badge/star.svg?theme=white ) | [代码生成器，自动写代码](https://gitee.com/mail_osc/writecode)  | 
| [log](https://gitee.com/mail_osc/log) | ![](https://gitee.com/mail_osc/log/badge/star.svg?theme=white ) | [Java日志存储及读取](https://gitee.com/mail_osc/log) | 
| [layui translate](https://gitee.com/mail_osc/translate_layui) |  ![](https://gitee.com/mail_osc/translate_layui/badge/star.svg?theme=white ) | [Layui的国际化支持组件](https://gitee.com/mail_osc/translate_layui) |
| [http.java](https://gitee.com/mail_osc/http.java) |  ![](https://gitee.com/mail_osc/http.java/badge/star.svg?theme=white ) | [Java8轻量级http请求类](https://gitee.com/mail_osc/http.java) |
| [xnx3](https://gitee.com/mail_osc/xnx3) |  ![](https://gitee.com/mail_osc/xnx3/badge/star.svg?theme=white ) | [Java版按键精灵，游戏辅助开发](https://gitee.com/mail_osc/xnx3) |  
| [websocket.js](https://gitee.com/mail_osc/websocket.js)  | ![](https://gitee.com/mail_osc/websocket.js/badge/star.svg?theme=white ) | [js的WebSocket框架封装](https://gitee.com/mail_osc/websocket.js) |
| [email.java](https://gitee.com/mail_osc/email.java) | ![](https://gitee.com/mail_osc/email.java/badge/star.svg?theme=white ) | [邮件发送](https://gitee.com/mail_osc/email.java) | 
| [notification.js](https://gitee.com/mail_osc/notification.js) | ![](https://gitee.com/mail_osc/notification.js/badge/star.svg?theme=white ) | [浏览器通知提醒工具类](https://gitee.com/mail_osc/notification.js) | 
| [pinyin.js](https://gitee.com/mail_osc/pinyin.js) | ![](https://gitee.com/mail_osc/pinyin.js/badge/star.svg?theme=white ) | [JS中文转拼音工具类](https://gitee.com/mail_osc/pinyin.js) |
| [xnx3_weixin](https://gitee.com/mail_osc/xnx3_weixin) | ![](https://gitee.com/mail_osc/xnx3_weixin/badge/star.svg?theme=white ) | [Java 微信常用工具类](https://gitee.com/mail_osc/xnx3_weixin) |
| [xunxian](https://gitee.com/mail_osc/xunxian) | ![](https://gitee.com/mail_osc/xunxian/badge/star.svg?theme=white ) | [QQ寻仙的游戏辅助软件](https://gitee.com/mail_osc/xunxian) | 
| [wangmarket_shop](https://gitee.com/leimingyun/wangmarket_shop) | ![](https://gitee.com/leimingyun/wangmarket_shop/badge/star.svg?theme=white ) | [私有化部署自己的 SAAS 商城](https://gitee.com/leimingyun/wangmarket_shop) |
| [wm](https://gitee.com/leimingyun/wm) | ![](https://gitee.com/leimingyun/wm/badge/star.svg?theme=white ) | [Java开发框架及规章约束](https://gitee.com/leimingyun/wm) |
| [yunkefu](https://gitee.com/leimingyun/yunkefu) | ![](https://gitee.com/leimingyun/yunkefu/badge/star.svg?theme=white ) | [私有化部署自己的SAAS客服系统](https://gitee.com/leimingyun/yunkefu) |
| [javadoc](https://gitee.com/leimingyun/javadoc) | ![](https://gitee.com/leimingyun/javadoc/badge/star.svg?theme=white) | [根据标准的 JavaDoc 生成接口文档 ](https://gitee.com/leimingyun/javadoc) |
| [elasticsearch util](https://gitee.com/leimingyun/elasticsearch) | ![](https://gitee.com/leimingyun/elasticsearch/badge/star.svg?theme=white ) | [用sql方式使用Elasticsearch](https://gitee.com/leimingyun/elasticsearch) |
| [AutoPublish](https://gitee.com/leimingyun/sftp-ssh-autopublish) | ![](https://gitee.com/leimingyun/sftp-ssh-autopublish/badge/star.svg?theme=white ) | [Java应用全自动部署及更新](https://gitee.com/leimingyun/sftp-ssh-autopublish) |
| [aichat](https://gitee.com/leimingyun/aichat) | ![](https://gitee.com/leimingyun/aichat/badge/star.svg?theme=white ) | [智能聊天机器人](https://gitee.com/leimingyun/aichat) | 
| [yunbackups](https://gitee.com/leimingyun/yunbackups) | ![](https://gitee.com/leimingyun/yunbackups/badge/star.svg?theme=white ) | [自动备份文件到云存储及FTP等](https://gitee.com/leimingyun/yunbackups) |
| [chatbot](https://gitee.com/leimingyun/chatbot) | ![](https://gitee.com/leimingyun/chatbot/badge/star.svg?theme=white) | [智能客服机器人](https://gitee.com/leimingyun/chatbot) |
| [java print](https://gitee.com/leimingyun/printJframe) | ![](https://gitee.com/leimingyun/printJframe/badge/star.svg?theme=white ) | [Java打印及预览的工具类](https://gitee.com/leimingyun/printJframe) |
…………